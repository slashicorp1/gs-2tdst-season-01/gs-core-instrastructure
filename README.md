# IaC com Gitlab-ci e Terraform 

![terraform](https://img.shields.io/badge/terraform-blueviolet?style=flat-square)
![aws cli](https://img.shields.io/badge/aws--cli-1.18.134-yellow?style=flat-square)
![kubectl](https://img.shields.io/badge/kubectl-1.19.3-blue?style=flat-square)
![aws iam](https://img.shields.io/badge/aws-iam-pourple?style=flat-square)
![ingress controller](https://img.shields.io/badge/ingress--controller-nginx-green?style=flat-square)
![prometheus](https://img.shields.io/badge/prometheus-latest-red?style=flat-square)
![gitlab runner](https://img.shields.io/badge/gitlab--runner-latest-violet?style=flat-square)

## O que preciso precisa para acompanhar este LAB?

- Conta no [gitlab.com](https://gitlab.com "gitlab.com");
- Conta [AWS](https://console.aws.amazon.com "AWS") com permissoes administrativas;
- aws cli instalado [documentação](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-chap-install.html "documentação");
- aws cli configurado [documentação](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-configure-quickstart.html "documentação");
- kubectl instalado [documentação](https://docs.aws.amazon.com/pt_br/eks/latest/userguide/install-kubectl.html "documentação").

## Quais variaveis não posso me esquecer de configurar?
*Criando Variables no projeto: [documentação](https://docs.gitlab.com/ee/ci/variables/README.html "documentação").*

- `AWS_ACCESS_KEY_ID` = Contendo o ID do usuario `IAM` com policy de gerencia do `EKS`.
- `AWS_SECRET_ACCESS_KEY` = Contendo a Secret key do usuario IAM.
- `AWS_DEFAULT_REGION` = ID da zona default escolhida para provisionamento do ambiente.

# Estrutura proposta

![Topologia proposta - aws](img/gs-slashicorp.jpeg "Topologia proposta - aws")

## Proposta para implementação de: VPC + SUBREDES + NW_COMPONENTS + EKS + RDS

Utilizando as facilitações oferecidas aos usuáários de Gitlab para versionamento de infraestrutura como código,
estaremos aplicando todo o nosso ambiente qual provisionaremos nossa API Rest para o desafio proposto pelo grupo
doscente da FIAP, responsáveis pelo curso/turma `2TDST`.

## Integrantes do grupo

```yml
RM85619: Allan Phyllyp Reis
RM84082: Dihogo Cassimiro Teixeira
RM85833: Fernando Borgatto Bouman
RM86486: Paloma Rangel Rocha
RM85468: Juan Carlos Benvive Serrano
```

Estaremos demonstrando e atendendo aos itens econtrados na proposta de `Cenário 01` que contempla em sua descrição:

# Cenário 01:

```
- Implementar a aplicação de software definida pelo seu grupo completa em nuvem, ou seja, App e Banco de Dados
- Nesse cenário, é esperado o link de acesso para testes da aplicação completa, ou na inviabilidade, os prints de acesso, confirmando a evidência do uso. 

Também um link de acesso dos códigos fontes no GitHub (em pdf)
```

# How-to pipeline ci/cd com Terraform

Realize o clone do repositório atual:

```sh
$ git clone https://gitlab.com/slashicorp1/gs-2tdst-season-01/gs-core-instrastructure.git
```

## Estrutura de diretório

```sh
$ cd gs-core-instrastructure
$ tree .
.
├── README.md
├── aws_db_instance.tf
├── main.tf
├── outputs.tf
├── provider.tf
├── terraform.tfvars
└── variables.tf
```

# IAM permissions, credenciais AWS para uso do Terraform

O usuário utilizado para provisionamento do EKS deverá possuir IAM permissions necessáária para provisionar e gerenciar todos os recursos que o EKS necessita para funcionar, consulte o exemplo de IAM user ou IAM role, utilizado neste exemplo clicando [aqui](https://github.com/terraform-aws-modules/terraform-aws-eks/blob/master/docs/iam-permissions.md "aqui"). 

O mesmo template de Role pode ser encontrado a baixo, consulte o manual da AWS de como criar Role de usuário ou de serviço

- IAM [Permissions](https://github.com/terraform-aws-modules/terraform-aws-eks/blob/master/docs/iam-permissions.md "Permissions"): Minimum IAM permissions needed to setup EKS Cluster.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "autoscaling:AttachInstances",
                "autoscaling:CreateAutoScalingGroup",
                "autoscaling:CreateLaunchConfiguration",
                "autoscaling:CreateOrUpdateTags",
                "autoscaling:DeleteAutoScalingGroup",
                "autoscaling:DeleteLaunchConfiguration",
                "autoscaling:DeleteTags",
                "autoscaling:Describe*",
                "autoscaling:DetachInstances",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:UpdateAutoScalingGroup",
                "autoscaling:SuspendProcesses",
                "ec2:AllocateAddress",
                "ec2:AssignPrivateIpAddresses",
                "ec2:Associate*",
                "ec2:AttachInternetGateway",
                "ec2:AttachNetworkInterface",
                "ec2:AuthorizeSecurityGroupEgress",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:CreateDefaultSubnet",
                "ec2:CreateDhcpOptions",
                "ec2:CreateEgressOnlyInternetGateway",
                "ec2:CreateInternetGateway",
                "ec2:CreateNatGateway",
                "ec2:CreateNetworkInterface",
                "ec2:CreateRoute",
                "ec2:CreateRouteTable",
                "ec2:CreateSecurityGroup",
                "ec2:CreateSubnet",
                "ec2:CreateTags",
                "ec2:CreateVolume",
                "ec2:CreateVpc",
                "ec2:CreateVpcEndpoint",
                "ec2:DeleteDhcpOptions",
                "ec2:DeleteEgressOnlyInternetGateway",
                "ec2:DeleteInternetGateway",
                "ec2:DeleteNatGateway",
                "ec2:DeleteNetworkInterface",
                "ec2:DeleteRoute",
                "ec2:DeleteRouteTable",
                "ec2:DeleteSecurityGroup",
                "ec2:DeleteSubnet",
                "ec2:DeleteTags",
                "ec2:DeleteVolume",
                "ec2:DeleteVpc",
                "ec2:DeleteVpnGateway",
                "ec2:Describe*",
                "ec2:DetachInternetGateway",
                "ec2:DetachNetworkInterface",
                "ec2:DetachVolume",
                "ec2:Disassociate*",
                "ec2:ModifySubnetAttribute",
                "ec2:ModifyVpcAttribute",
                "ec2:ModifyVpcEndpoint",
                "ec2:ReleaseAddress",
                "ec2:RevokeSecurityGroupEgress",
                "ec2:RevokeSecurityGroupIngress",
                "ec2:UpdateSecurityGroupRuleDescriptionsEgress",
                "ec2:UpdateSecurityGroupRuleDescriptionsIngress",
                "ec2:CreateLaunchTemplate",
                "ec2:CreateLaunchTemplateVersion",
                "ec2:DeleteLaunchTemplate",
                "ec2:DeleteLaunchTemplateVersions",
                "ec2:DescribeLaunchTemplates",
                "ec2:DescribeLaunchTemplateVersions",
                "ec2:GetLaunchTemplateData",
                "ec2:ModifyLaunchTemplate",
                "ec2:RunInstances",
                "eks:CreateCluster",
                "eks:DeleteCluster",
                "eks:DescribeCluster",
                "eks:ListClusters",
                "eks:UpdateClusterConfig",
                "eks:UpdateClusterVersion",
                "eks:DescribeUpdate",
                "eks:TagResource",
                "eks:UntagResource",
                "eks:ListTagsForResource",
                "eks:CreateFargateProfile",
                "eks:DeleteFargateProfile",
                "eks:DescribeFargateProfile",
                "eks:ListFargateProfiles",
                "eks:CreateNodegroup",
                "eks:DeleteNodegroup",
                "eks:DescribeNodegroup",
                "eks:ListNodegroups",
                "eks:UpdateNodegroupConfig",
                "eks:UpdateNodegroupVersion",
                "iam:AddRoleToInstanceProfile",
                "iam:AttachRolePolicy",
                "iam:CreateInstanceProfile",
                "iam:CreateOpenIDConnectProvider",
                "iam:CreateServiceLinkedRole",
                "iam:CreatePolicy",
                "iam:CreatePolicyVersion",
                "iam:CreateRole",
                "iam:DeleteInstanceProfile",
                "iam:DeleteOpenIDConnectProvider",
                "iam:DeletePolicy",
                "iam:DeletePolicyVersion",
                "iam:DeleteRole",
                "iam:DeleteRolePolicy",
                "iam:DeleteServiceLinkedRole",
                "iam:DetachRolePolicy",
                "iam:GetInstanceProfile",
                "iam:GetOpenIDConnectProvider",
                "iam:GetPolicy",
                "iam:GetPolicyVersion",
                "iam:GetRole",
                "iam:GetRolePolicy",
                "iam:List*",
                "iam:PassRole",
                "iam:PutRolePolicy",
                "iam:RemoveRoleFromInstanceProfile",
                "iam:TagOpenIDConnectProvider",
                "iam:TagRole",
                "iam:UntagRole",
                "iam:UpdateAssumeRolePolicy",
                // As seguintes permissões são necessárias se cluster_enabled_log_types estiver enabled
                "logs:CreateLogGroup",
                "logs:DescribeLogGroups",
                "logs:DeleteLogGroup",
                "logs:ListTagsLogGroup",
                "logs:PutRetentionPolicy",
                // Seguindo as permissões para trabalhar com secrets_encryption example
                "kms:CreateAlias",
                "kms:CreateGrant",
                "kms:CreateKey",
                "kms:DeleteAlias",
                "kms:DescribeKey",
                "kms:GetKeyPolicy",
                "kms:GetKeyRotationStatus",
                "kms:ListAliases",
                "kms:ListResourceTags",
                "kms:ScheduleKeyDeletion"
            ],
            "Resource": "*"
        }
    ]
}
```

# Fornecendo credenciais para o Terraform

Altere o arquivo `terraform.tfvars` dentro do seu repositório clonado `local`, alterando as variáveis por `Strings` contendo:

```tf
aws_access_key = "${AWS_ACCESS_KEY}" 
aws_secret_key = "${AWS_SECRET_KEY}"
aws_region     = "${AWS_DEFAULT_REGION}"
```

Ex.: 

```tf
aws_access_key = "YOUR_AWS_ACCESS_KEY_ID"
aws_secret_key = "YOUR_AWS_SECRET_ACCESS_KEY"
aws_region     = "us-east-1"
```

# Iniciando o provisionamento

Para que o processo de build se inicie, o arquivo principal para funcionamento do CI deveráá estar na raíz do projeto com o nome: `.gitlab-ci.yml` nós jáá disponibilizamos um neste projeto, para que ele seja "acionado" e a pipeline "corra", basta realizar um `PR` (Pull Request) para sua branch `main` em seu reposiório remoto no `Gitlab.com`, caso esteja trabalhando com `gitflow` de uma branch `develop` por exemplo.
Caso esteja trabalhando diretamente na branch `main`, a funcionalidade de [AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/ "AutoDevOps") do Gitlab, ao enxergar a existêência do arquivo `.gitlab-ci.yml` irá correr todos os stages necessáários para provisionar a infraestrutura, o stage de `deploy` e `apply` está disponíível apenas na branch `main`, caso queira alterar de maneira a possibilitar que ele esteja visível de outra ramificação, altere a seguinte linha de código no arquivo `.gitlab-ci.yml` como no exemplo:

## Definição de parâmetros importantes:

- `when: manual`: Define que esse stage nãão pode correr de forma automáática, necessário aprovaçãão do mantenedor.
- `only`: Define qual ou quais branchs poderão correr esse stage deploy.

`.gitlab-ci.yml`
```yml
apply:
  stage: deploy
  environment:
    name: production
  script:
    - gitlab-terraform apply
  dependencies:
    - plan
  when: manual
  only: 
    - main
```

## Acompanhando esteira CI/cd

Para ter acesso ao log dos `stages` escritos no arquivo `.gitlab-ci.yml` em tempo de execução, siga os seguintes passos em seu repositóório:

Clique no menu esquerdo `CI/CD` > `Pipelines`
![MenuPipelines](img/cicd-menu.png "MenuPipelines") 

Seráá possíível visualizar o historico de pipelines corridas e acompanhar o status de alguma que esteja correndo:
![pipelineHistoryStatus](img/pipeline_history_status.png "pipelineHistoryStatus")

Para visualizar o status das jobs correndo dentro de sua pipeline, clicando no icone de `status`:
![viewStatusJob](img/view_status_job.png "viewStatusJob")

Ao clicar em alguma `job` é possivel visualizar o log do `gitlab-runner` em tempo de execução:
![viewJobLog](img/view_job_log.png "viewJobLog")

# Evidências

Cluster EKS `ready` funcional:

![eks](img/eks_validation.png "eks")

Log de Boostrap do `POD` da aplicação iniciando no `kubernetes` construido no repo https://gitlab.com/slashicorp1/gs-2tdst-season-01/gs-core-instrastructure.git com `Terraform` e `gitlab-ci`:

![bootstrap_pod](img/api_bootstrap_log_eks.png "bootstrap_pod")

Evidencia de criaçãão do RDS na AWS:

![rds_create](img/RDS_CREATE.png "rds_create")

Teste de conexao com o banco de dados MySQL `RDS` provisionado no repo https://gitlab.com/slashicorp1/gs-2tdst-season-01/gs-core-instrastructure.git com `Terraform` e `gitlab-ci`:

![db_connect](img/TEST_CONNECT_DB.png "db_connect")
