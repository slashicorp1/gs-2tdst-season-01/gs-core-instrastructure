data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_availability_zones" "available" {
}

resource "random_string" "suffix" {
  length  = 4
  special = false
}

locals {
  environment = random_string.suffix.result
  name   = "gs-slashicorp-${random_string.suffix.result}"
  cluster_name = "eks-slashicorp-${random_string.suffix.result}"
  region = var.aws_region
  deploy = "${random_string.suffix.result}"
  tags = {
    Owner       = "FIAP"
    Vendor      = "SLASHICORP"
    Environment = "Production"
    Terraform   = "true"
    Deploy      = "${random_string.suffix.result}"
  }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.0.0"

  name    = local.name
  cidr                 = "10.14.0.0/16"
  azs                  = ["${local.region}a", "${local.region}b", "${local.region}c"]
  private_subnets      = ["10.14.1.0/24", "10.14.2.0/24", "10.14.3.0/24"]
  public_subnets       = ["10.14.4.0/24", "10.14.5.0/24", "10.14.6.0/24"]
  enable_nat_gateway   = true
  single_nat_gateway   = false
  one_nat_gateway_per_az = true
  enable_dns_hostnames = true
  enable_dns_support   = true
  enable_dhcp_options              = true
  dhcp_options_domain_name         = "ec2.internal"

  tags = local.tags
  
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version = "16.2.0"

  cluster_name    = local.cluster_name
  cluster_version = "1.19"
  subnets         = module.vpc.public_subnets
  worker_create_cluster_primary_security_group_rules = true

  tags = local.tags

  vpc_id = module.vpc.vpc_id

  node_groups_defaults = {
    ami_type  = "AL2_x86_64"
    disk_size = 40
  }

  node_groups = {
    valecard = {
      desired_capacity = 2
      max_capacity     = 4
      min_capacity     = 2

      instance_types = ["t3.medium"]
      capacity_type  = "SPOT"
      k8s_labels = {
        Environment = "Production"
      }
    }
  }
}
